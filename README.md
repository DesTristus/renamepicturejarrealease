This jar plugin is a dependancy of ImageJ/fiji software. The main goal is to rename set of images with same caracteristics/experiences with unique identification.
Set of images have to be in the same folder, and the name of this folder need to contain the liste of informations you want to add to the images separate by underscore. The name folder will be add before the image name and after the the second underscore of the desciption the time stamp of the image creation will be add.
Exemple of a folder named Z_Col_cot13&14&18__w11 DAPI SIM containing the following image will be rename like this :

S2.tif -> Z_1527149020074_Col_cot13&14&18__w11 DAPI SIM_s2.TIF

1527149020074 is the converstion in second of the time of the creation of the image S2.tif. (Note that the 2 last numbers correspond to millisecond)

Actually the plugin works only for tif and czi format.


To install the plugin :
* [Download and save the jar here](https://gitlab.com/DesTristus/renamepicturejarrealease/blob/master/renamepictureplugin_.jar)
* The add this jar in the plugins folder of ImageJ/fiji software
 
**Linux usage :**

1 - Run your ImageJ/fiji software

2 - Click in plugins menu select : RenameImage/Rename files

3 - the following windows will appear :

![Alt-Text](images/rename_file.png)

4 - You can then select the folder name containing images you want to rename and click on run.

**Windows and mac usage :**

Step 1 -4 like linux but the plugin will generate a TSV file with the same name of the folder at the same tree height :

5 - Click in plugins menu select : RenameImage/Rename files TSV

6 - Select the TSV file and click on run.

![Alt-Text](images/TSV_file.png)
